# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [3.2.3] - 2020-02-20

### Fixed

* Fixed a bug in batch insert when first row contains null value

## [3.2.2] - 2020-02-01

### Fixed

* Fix an exception when using ao batch insertion against MySQL

### Changed
* Fixed quoting in group by/aliased queries - previously the whole clause was quoted, rather than the column name and alias name

## [3.2.0] - 2019-12-17

### Changed

* Add support for batch insert

## [3.1.9] - 2020-01-20

### Changed

* Update readme with instructions for releasing branches 3.1.x or later.

## [3.1.8] - 2020-01-17

### Fixed
* [AO-3510], [BSP-893] : Update MySQL Reserved words to support MySQL 8.0.x.

## [3.1.2] - 2019-07-02

### Changed

* [BSP-268](https://bulldog.internal.atlassian.com/browse/BSP-268): Change AO to not use Sonatype OSS parent POM.
* [AO-3479](https://ecosystem.atlassian.net/browse/AO-3479): Ability to set transaction isolation level.

### Fixed 

* [RAID-1093](https://bulldog.internal.atlassian.com/browse/RAID-1093): SECURITY - Activity Stream Gadget causing high memory/CPU consumption JRASERVER-65826.

## [3.1.0] - 2019-03-05

### Fixed

* [RAID-1093](https://bulldog.internal.atlassian.com/browse/RAID-1093): SECURITY - Activity Stream Gadget causing high memory/CPU consumption JRASERVER-65826.
* [BSP-351](https://bulldog.internal.atlassian.com/browse/BSP-351): Fix AO cross-db build.
* [BSP-271](https://bulldog.internal.atlassian.com/browse/BSP-271): Fix README and information for all modules.

## [3.0.1] - 2020-04-07

### Fixed

* [AO-3521](https://ecosystem.atlassian.net/browse/AO-3521): AO distinct query fails on columns with different type to the primary key.

## [3.0.0] - 2018-12-10

### Changed

* [AO-3459](https://ecosystem.atlassian.net/browse/AO-3459): Make project compatible with Java 11.

### Fixed

* [AO-3468](https://ecosystem.atlassian.net/browse/AO-3468): Deadlock when inserting entities on MSSQL
* [AO-3464](https://ecosystem.atlassian.net/browse/AO-3464): Parsed DDLField for Oracle's DOUBLE PRECISION has wrong jdbc type which leads to failed comparison with read DDLField which leads to repeated (each startup) execution of unnecessary ALTER TABLE expressions.
* [AO-3463](https://ecosystem.atlassian.net/browse/AO-3463): AO fails to parse default value for Oracle's while using @NotNull and @Default annotations which leads to repeated (each startup) execution of unnecessary ALTER TABLE expressions. 
* [AO-3460](https://ecosystem.atlassian.net/browse/AO-3460): AO parses default value for Oracle's NUMBER(20,0) using wrong Java class which leads to repeated (each startup) execution of unnecessary ALTER TABLE expressions. 

## [2.1.0] - 2019-03-27

### Changed

* [AO-3479]: Transaction isolation level used by `net.java.ao.DatabaseProvider#startTransaction` is configurable using 
system property `ao.transaction.isolation.level`

[2.1.0]: https://bitbucket.org/activeobjects/ao/commits/tag/v2.1.0

## [2.0.0] - 2018-07-17

### Fixed

* [AO-3406]: Fixed unnecessary migrations for LONGTEXT datatype in MySQL DB

### Removed

* [AO-3406]: Removed automatic migration from TEXT to LONGTEXT in MySQL DB introduced in [AO-396].
Most of AO users have already performed such migration, others will need to do it manually (via a backup/restore).

[2.0.0]: https://bitbucket.org/activeobjects/ao/commits/tag/v2.0.0
[AO-3406]: https://ecosystem.atlassian.net/browse/AO-3406
[AO-396]: https://ecosystem.atlassian.net/browse/AO-396

## [1.6.0] - 2019-03-27

### Changed
* [AO-3479]: Transaction isolation level used by `net.java.ao.DatabaseProvider#startTransaction` is configurable using 
system property `ao.transaction.isolation.level`

[1.6.0]: https://bitbucket.org/activeobjects/ao/commits/tag/v1.6.0
[AO-3479]: https://ecosystem.atlassian.net/browse/AO-3479   

## [1.5.2] - 2018-06-13

### Fixed

* [AO-3460]: Fixed unnecessary migrations for nullable numeric types with default values in Oracle DB
* [AO-3463]: Fixed unnecessary migrations for columns with Default NotNull modifiers in Oracle DB
* [AO-3464]: Started using FLOAT(126) instead of DOUBLE PRECISION in Oracle. This fixes unnecessary migrations during AO startup
* [AO-3468]: Fixed deadlock when inserting entities on MSSQL

[1.5.2]: https://bitbucket.org/activeobjects/ao/commits/tag/v1.5.2
[AO-3460]: https://ecosystem.atlassian.net/browse/AO-3460
[AO-3463]: https://ecosystem.atlassian.net/browse/AO-3463
[AO-3464]: https://ecosystem.atlassian.net/browse/AO-3464
[AO-3468]: https://ecosystem.atlassian.net/browse/AO-3468
