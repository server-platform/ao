# Active Objects

# Repository was moved

This repository was moved to a new location - [Active Objects](https://bitbucket.org/server-platform/activeobjects)

## Description

Active Objects is an ORM (object relational mapping) layer in Atlassian products, provided via the
[Active Objects Plugin](https://bitbucket.org/server-platform/ao-plugin/).

The active objects project uses maven for its build. To build:

1. Install maven (3.3.3 or above):
    - you can find all about maven at [maven.apache.org](http://maven.apache.org/)
    - install instructions are [here](http://maven.apache.org/download.html#Installation)

2. Use the libraries from the ```repository``` directory by either:
    - Adding this directory as a repository to your maven configuration, or
    - Copying the content of that repository to your local maven repository: ```~/.m2/repository```

3. From the root of the project, run: ```mvn install```

4. That's it! You will find the active objects library under ```${basedir}/activeobjects/target/activeobject-<version>.jar```

## Ownership

This project is owned by the Server Java Platform team (go/abracadabra).

## Atlassian Developer?

### Internal Documentation

* [Development and maintenance documentation](https://ecosystem.atlassian.net/wiki/display/AO/Home)

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/AOLIB)

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/AO)

### Documentation

[Active Objects Documentation](https://developer.atlassian.com/display/DOCS/Active+Objects)

### Releasing

This library and the AO plugin should always be released in lockstep, even if only one of them has actual changes. This
is what the product teams expect. 

To run the automated release from bamboo if working on 3.1.x or later, you need to run the the [Cross Database Build](https://ecosystem-bamboo.internal.atlassian.com/browse/AOLIB-CD3) 
plan for the release branch to perform the release. 

So if you need to change:
####the library:
 
   * release a new version of it,
   * bump this plugin to depend on that version of the library (see `ao.version`),
   * bump this plugin's own version to be that same version, and
   * release this plugin.
   
####just the plugin:
 
   * release a new version of the library (whether it's changed or not),
   * bump this plugin to depend on that new version of the library (see `ao.version`),
   * bump this plugin's own version to be that same version, and
   * release this plugin.
