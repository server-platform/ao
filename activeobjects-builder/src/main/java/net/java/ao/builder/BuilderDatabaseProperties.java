package net.java.ao.builder;


import java.util.Objects;

final class BuilderDatabaseProperties implements DatabaseProperties {
    private final String url;
    private final String username;
    private final String password;
    private final ConnectionPool pool;
    private String schema = null;

    public BuilderDatabaseProperties(String url, String username, String password, ConnectionPool pool) {
        this.url = Objects.requireNonNull(url, "url can't be null");
        this.username = Objects.requireNonNull(username, "username can't be null");
        this.password = Objects.requireNonNull(password, "password can't be null");
        this.pool = Objects.requireNonNull(pool, "pool can't be null");
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public ConnectionPool getConnectionPool() {
        return pool;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }
}
