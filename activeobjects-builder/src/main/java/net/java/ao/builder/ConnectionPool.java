package net.java.ao.builder;

import net.java.ao.ActiveObjectsException;
import net.java.ao.Disposable;
import net.java.ao.DisposableDataSource;
import net.java.ao.builder.c3po.C3poDataSourceFactory;
import net.java.ao.builder.dbcp.DbcpDataSourceFactory;
import net.java.ao.builder.dbpool.DbPoolDataSourceFactory;
import net.java.ao.builder.proxool.ProxoolDataSourceFactory;

import java.lang.reflect.InvocationTargetException;
import java.sql.Driver;
import java.util.Objects;


public enum ConnectionPool implements DataSourceFactory {
    C3PO(C3poDataSourceFactory.class),
    DBPOOL(DbPoolDataSourceFactory.class),
    PROXOOL(ProxoolDataSourceFactory.class),
    DBCP(DbcpDataSourceFactory.class),
    NONE(null) {
        @Override
        public boolean isAvailable() {
            return true;
        }

        @Override
        public DisposableDataSource getDataSource(Class<? extends Driver> driverClass, String url, String username, String password) {
            return DelegatingDisposableDataSourceHandler.newInstance(
                    new DriverManagerDataSource(url, username, password),
                    new Disposable() {
                        public void dispose() {
                        }
                    });
        }
    };

    private final Class<? extends DataSourceFactory> dataSourceFactoryClass;

    ConnectionPool(Class<? extends DataSourceFactory> dataSourceFactoryClass) {
        this.dataSourceFactoryClass = dataSourceFactoryClass;
    }

    public DisposableDataSource getDataSource(Class<? extends Driver> driverClass, String url, String username, String password) {
        Objects.requireNonNull(dataSourceFactoryClass, "dataSourceFactoryClass can't be null");
        try {
            return dataSourceFactoryClass.newInstance().getDataSource(driverClass, url, username, password);
        } catch (InstantiationException | IllegalAccessException e) {
            throw new ActiveObjectsException("Could not create an instance of <" + dataSourceFactoryClass + ">, have you called isAvailable before hand?", e);
        }
    }

    public boolean isAvailable() {
        Objects.requireNonNull(dataSourceFactoryClass, "dataSourceFactoryClass can't be null");
        try {
            return (Boolean) dataSourceFactoryClass.getMethod("isAvailable").invoke(null);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            return false;
        }
    }
}
