package net.java.ao.builder;

import net.java.ao.EntityManager;

import static net.java.ao.builder.DatabaseProviderFactory.getDatabaseProvider;

/**
 * This is class used to build {@link net.java.ao.EntityManager}
 *
 * @see EntityManagerBuilder
 * @see EntityManagerBuilderWithUrl
 * @see EntityManagerBuilderWithUrlAndUsername
 */
public final class EntityManagerBuilderWithDatabaseProperties extends AbstractEntityManagerBuilderWithDatabaseProperties<EntityManagerBuilderWithDatabaseProperties> {
    EntityManagerBuilderWithDatabaseProperties(BuilderDatabaseProperties databaseProperties) {
        super(databaseProperties);
    }

    public EntityManager build() {
        return new EntityManager(getDatabaseProvider(getDatabaseProperties()), getEntityManagerConfiguration());
    }
}
