package net.java.ao;

import net.java.ao.sql.LoggingInterceptor;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Date;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

class DelegateLoggingPreparedStatement extends DelegatePreparedStatement {
    private String query;

    DelegateLoggingPreparedStatement(PreparedStatement statement, LoggingInterceptor logger, String query) {
        super(statement, logger);

        this.query = query;
    }

    @FunctionalInterface
    protected interface DelegateVoidBlock {
        void invoke() throws SQLException;
    }

    private void delegateSetParam(int index, String value, DelegateVoidBlock block) throws SQLException {
        try {
            block.invoke();

            params.put(index, value);
        } catch (SQLException e) {
            logger.onException(e);
            throw e;
        }
    }

    @Override
    public boolean execute() throws SQLException {
        return delegateExecute(query, preparedStatement::execute);
    }

    @Override
    public void addBatch() throws SQLException {
        preparedStatement.addBatch();

        if (batchQueryBuffer.length() > 0) {
            batchQueryBuffer.append("\n");
        }

        batchQueryBuffer.append(query);


        if (!params.isEmpty()) {
            batchQueryBuffer.append(" ")
                    .append(params.toString());
        }
    }

    @Override
    public long executeLargeUpdate() throws SQLException {
        return delegateExecute(query, preparedStatement::executeLargeUpdate);
    }

    @Override
    public ResultSet executeQuery() throws SQLException {
        return delegateExecute(query, preparedStatement::executeQuery);
    }

    @Override
    public int executeUpdate() throws SQLException {
        return delegateExecute(query, preparedStatement::executeUpdate);
    }

    @Override
    public void setArray(int parameterIndex, Array x) throws SQLException {
        preparedStatement.setArray(parameterIndex, x);
    }

    @Override
    public ResultSetMetaData getMetaData() throws SQLException {
        return preparedStatement.getMetaData();
    }

    @Override
    public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setDate(parameterIndex, x, cal));
    }

    @Override
    public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setTime(parameterIndex, x, cal));
    }

    @Override
    public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setTimestamp(parameterIndex, x, cal));
    }

    @Override
    public void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException {
        delegateSetParam(parameterIndex, "null", () -> preparedStatement.setNull(parameterIndex, sqlType, typeName));
    }

    @Override
    public void setURL(int parameterIndex, URL x) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setURL(parameterIndex, x));
    }

    @Override
    public ParameterMetaData getParameterMetaData() throws SQLException {
        return preparedStatement.getParameterMetaData();
    }

    @Override
    public void setRowId(int parameterIndex, RowId x) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setRowId(parameterIndex, x));
    }

    @Override
    public void setNString(int parameterIndex, String value) throws SQLException {
        delegateSetParam(parameterIndex, value, () -> preparedStatement.setNString(parameterIndex, value));
    }

    @Override
    public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
        delegateSetParam(parameterIndex, xmlObject.getString(), () -> preparedStatement.setSQLXML(parameterIndex, xmlObject));
    }

    @Override
    public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setObject(parameterIndex, x, targetSqlType, scaleOrLength));
    }

    @Override
    public void setObject(int parameterIndex, Object x, SQLType targetSqlType, int scaleOrLength) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setObject(parameterIndex, x, targetSqlType, scaleOrLength));
    }

    @Override
    public void setObject(int parameterIndex, Object x, SQLType targetSqlType) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setObject(parameterIndex, x, targetSqlType));
    }

    @Override
    public void setNull(int parameterIndex, int sqlType) throws SQLException {
        delegateSetParam(parameterIndex, "null", () -> preparedStatement.setNull(parameterIndex, sqlType));
    }

    @Override
    public void setBoolean(int parameterIndex, boolean x) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setBoolean(parameterIndex, x));
    }

    @Override
    public void setShort(int parameterIndex, short x) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setShort(parameterIndex, x));
    }

    @Override
    public void setInt(int parameterIndex, int x) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setInt(parameterIndex, x));
    }

    @Override
    public void setLong(int parameterIndex, long x) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setLong(parameterIndex, x));
    }

    @Override
    public void setFloat(int parameterIndex, float x) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setFloat(parameterIndex, x));
    }

    @Override
    public void setDouble(int parameterIndex, double x) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setDouble(parameterIndex, x));
    }

    @Override
    public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setBigDecimal(parameterIndex, x));
    }

    @Override
    public void setString(int parameterIndex, String x) throws SQLException {
        delegateSetParam(parameterIndex, x, () -> preparedStatement.setString(parameterIndex, x));
    }

    @Override
    public void setDate(int parameterIndex, Date x) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setDate(parameterIndex, x));
    }

    @Override
    public void setTime(int parameterIndex, Time x) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setTime(parameterIndex, x));
    }

    @Override
    public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setTimestamp(parameterIndex, x));
    }

    @Override
    public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setObject(parameterIndex, x, targetSqlType));
    }

    @Override
    public void setObject(int parameterIndex, Object x) throws SQLException {
        delegateSetParam(parameterIndex, String.valueOf(x), () -> preparedStatement.setObject(parameterIndex, x));
    }

    @Override
    public void clearParameters() throws SQLException {
        preparedStatement.clearParameters();
    }
}