package net.java.ao.schema;

import net.java.ao.RawEntity;

import java.util.Objects;

public abstract class CanonicalClassNameTableNameConverter implements TableNameConverter {
    public final String getName(Class<? extends RawEntity<?>> entityClass) {
        return getName(Objects.requireNonNull(entityClass).getCanonicalName());
    }

    protected abstract String getName(String entityClassCanonicalName);
}