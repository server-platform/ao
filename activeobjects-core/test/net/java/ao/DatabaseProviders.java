package net.java.ao;

import net.java.ao.db.EmbeddedDerbyDatabaseProvider;
import net.java.ao.db.H2DatabaseProvider;
import net.java.ao.db.HSQLDatabaseProvider;
import net.java.ao.db.MySQLDatabaseProvider;
import net.java.ao.db.NuoDBDatabaseProvider;
import net.java.ao.db.OracleDatabaseProvider;
import net.java.ao.db.PostgreSQLDatabaseProvider;
import net.java.ao.db.SQLServerDatabaseProvider;
import net.java.ao.schema.IndexNameConverter;
import net.java.ao.schema.ddl.DDLIndex;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DatabaseProviders {
    public static HSQLDatabaseProvider getHsqlDatabaseProvider(String quote) {
        return new HSQLDatabaseProvider(newDataSource(quote));
    }

    public static H2DatabaseProvider getH2DatabaseProvider(String quote) {
        return new H2DatabaseProvider(newDataSource(quote));
    }

    public static PostgreSQLDatabaseProvider getPostgreSqlDatabaseProvider(String quote) {
        return new PostgreSQLDatabaseProvider(newDataSource(quote)) {
            @Override
            protected boolean hasIndex(IndexNameConverter indexNameConverter, DDLIndex index) {
                return true;
            }

            @Override
            protected boolean hasIndex(String table, String index) {
                return true;
            }
        };
    }

    public static OracleDatabaseProvider getOracleDatabaseProvider(String quote) {
        return new OracleDatabaseProvider(newDataSource(quote));
    }

    public static NuoDBDatabaseProvider getNuoDBDatabaseProvider(String quote) {
        return new NuoDBDatabaseProvider(newDataSource(quote)) {
            @Override
            protected boolean hasIndex(IndexNameConverter indexNameConverter, DDLIndex index) {
                return true;
            }

            @Override
            protected boolean hasIndex(String table, String index) {
                return true;
            }
        };
    }

    public static MySQLDatabaseProvider getMySqlDatabaseProvider(final String quote) {
        return new MySQLDatabaseProvider(newDataSource(quote)) {
            @Override
            protected boolean hasIndex(IndexNameConverter indexNameConverter, DDLIndex index) {
                return true;
            }

            @Override
            protected boolean hasIndex(String table, String index) {
                return true;
            }
        };
    }

    public static SQLServerDatabaseProvider getMsSqlDatabaseProvider(String quote) {
        return new SQLServerDatabaseProvider(newDataSource(quote)) {
            @Override
            protected boolean hasIndex(IndexNameConverter indexNameConverter, DDLIndex index) {
                return true;
            }

            @Override
            protected boolean hasIndex(String table, String index) {
                return true;
            }
        };
    }

    public static EmbeddedDerbyDatabaseProvider getEmbeddedDerbyDatabaseProvider(final String quote) {
        return new EmbeddedDerbyDatabaseProvider(newDataSource(quote), "") {
            @Override
            protected void setPostConnectionProperties(Connection conn) {
                // nothing
            }
        };
    }

    public static DatabaseProvider getDatabaseProviderWithNoIndex() {
        return new DatabaseProvider(newDataSource(""), "") {
            @Override
            protected Set<String> getReservedWords() {
                return null;
            }

            @Override
            protected boolean hasIndex(String tableName, String indexName) {
                return false;
            }
        };
    }

    private static DisposableDataSource newDataSource(String quote) {
        final DisposableDataSource dataSource = mock(DisposableDataSource.class);
        final Connection connection = mock(Connection.class);
        final DatabaseMetaData metaData = mock(DatabaseMetaData.class);
        try {
            when(dataSource.getConnection()).thenReturn(connection);
            when(connection.getMetaData()).thenReturn(metaData);
            when(metaData.getIdentifierQuoteString()).thenReturn(quote);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return dataSource;
    }
}
