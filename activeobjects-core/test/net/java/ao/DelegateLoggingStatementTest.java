package net.java.ao;

import net.java.ao.sql.LoggingInterceptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DelegateLoggingStatementTest {

    @Mock
    private LoggingInterceptor mockedLoggingInterceptor;

    @Mock
    private Statement mockedStatement;
    private Statement statement;

    @Before
    public void setUp() {
        statement = new DelegateLoggingStatement(mockedStatement, mockedLoggingInterceptor);
    }

    @Test
    public void shouldLogSuccessfulStatementExecution() throws SQLException {
        statement.execute("some sql where id = ?");

        verify(mockedLoggingInterceptor).beforeExecution();
        verify(mockedLoggingInterceptor).afterSuccessfulExecution("some sql where id = ?", Collections.emptyMap());
        verify(mockedLoggingInterceptor, never()).onException(any(), any(), any());
    }

    @Test
    public void shouldLogStatementException() throws SQLException {
        SQLException exception = new SQLException();
        when(mockedStatement.execute(anyString())).thenThrow(exception);

        try {
            statement.execute("some sql where id = ?");
        } catch (final SQLException ignored) {
        }

        verify(mockedLoggingInterceptor).beforeExecution();
        verify(mockedLoggingInterceptor, never()).afterSuccessfulExecution(any(), any());
        verify(mockedLoggingInterceptor).onException("some sql where id = ?", Collections.emptyMap(), exception);
    }
}
