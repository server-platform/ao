package net.java.ao.db;

import net.java.ao.DatabaseProvider;
import org.junit.Test;

import static net.java.ao.DatabaseProviders.getEmbeddedDerbyDatabaseProvider;

public final class DerbyDatabaseProviderTest extends DatabaseProviderTest {
    @Override
    protected String getDatabase() {
        return "derby";
    }

    @Override
    protected DatabaseProvider getDatabaseProvider(String quote) {
        return getEmbeddedDerbyDatabaseProvider(quote);
    }

    @Test
    public void testRenderActionAlterColumn() {
        testRenderAction(new String[0], createActionAlterColumn, getDatabaseProvider());
    }

    @Test
    public void testRenderActionAlterStringColumn() {
        testRenderAction(new String[0], createActionAlterStringColumn, getDatabaseProvider());
    }

    @Test
    public void testRenderActionAlterStringLengthWithinBoundsColumn() {
        testRenderAction(new String[0], createActionAlterStringLengthColumn, getDatabaseProvider());
    }

    @Test
    public void testRenderActionAlterStringGreaterThanMaxLengthToUnlimitedColumn() {
        testRenderAction(new String[0], createActionAlterStringMaxLengthColumn, getDatabaseProvider());
    }

    @Test
    public void testRenderActionAlterNumericColumn() {
        testRenderAction(new String[0], createActionAlterNumericColumn, getDatabaseProvider());
    }

    @Test
    public void testRenderActionDropColumn() {
        testRenderAction(new String[0], createActionDropColumn, getDatabaseProvider());
    }
}
