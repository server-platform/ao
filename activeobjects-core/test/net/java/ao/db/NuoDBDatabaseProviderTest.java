package net.java.ao.db;

import net.java.ao.DatabaseProvider;

import static net.java.ao.DatabaseProviders.getNuoDBDatabaseProvider;

public final class NuoDBDatabaseProviderTest extends DatabaseProviderTest {
    @Override
    protected String getDatabase() {
        return "nuodb";
    }

    @Override
    protected DatabaseProvider getDatabaseProvider(String quote) {
        return getNuoDBDatabaseProvider(quote);
    }

    @Override
    protected String getExpectedProcessedMetadataNoRestrictedKeywordsWithAliasNoAggregateResult() {
        return "somedata as someotherdata";
    }

    @Override
    protected String getExpectedProcessedMetadataNoRestrictedWithAliasAndAggregate() {
        return "count(somedata) as someotherdata";
    }

    @Override
    protected String getExpectedProcessedMetadataWithRestrictedNoAliasWithAggregate() {
        return "count(somedata)";
    }

    @Override
    protected String getExpectedProcessedMetadataRestrictedKeywordsNoAliasNoAggregateResult() {
        return "`select`";
    }

    @Override
    protected String getExpectedProcessedMetadataRestrictedKeywordsAliasNoAggregateResult() {
        return "`select` as `select`";
    }

    @Override
    protected String getExpectedProcessedMetadataRestrictedKeywordsAliasAggregateResult() {
        return "count(`select`) as `select`";
    }

    @Override
    protected String getExpectedProcessedMetadataWithRestrictedNoAliasWithAggregateResult() {
        return "count(`select`)";
    }
}
