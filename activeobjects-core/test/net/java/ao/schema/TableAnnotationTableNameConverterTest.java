package net.java.ao.schema;

import net.java.ao.RawEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public final class TableAnnotationTableNameConverterTest {
    private TableNameConverter tableAnnotationTableNameConverter;

    @Mock
    private TableNameConverter delegateTableNameConverter;

    @Before
    public void setUp() {
        tableAnnotationTableNameConverter = new TableAnnotationTableNameConverter(delegateTableNameConverter);
    }

    @Test
    public void getNameForClassWithTableAnnotationAndAValidAnnotationValue() {
        assertEquals(EntityWithTableAnnotationAndValidAnnotationValue.VALID_TABLE_NAME,
                tableAnnotationTableNameConverter.getName(EntityWithTableAnnotationAndValidAnnotationValue.class));
    }

    @Test(expected = IllegalStateException.class)
    public void getNameForClassWithTableAnnotationAndEmptyAnnotationValue() {
        tableAnnotationTableNameConverter.getName(EntityWithTableAnnotationAndEmptyAnnotationValue.class);
    }

    @Test
    public void getNameForClassWithNoTableAnnotation() {
        tableAnnotationTableNameConverter.getName(EntityWithNoTableAnnotation.class);
        verify(delegateTableNameConverter).getName(EntityWithNoTableAnnotation.class);
    }

    @Table(EntityWithTableAnnotationAndValidAnnotationValue.VALID_TABLE_NAME)
    private interface EntityWithTableAnnotationAndValidAnnotationValue extends RawEntity<Object> {
        String VALID_TABLE_NAME = "ValidTableName";
    }

    @Table("")
    private interface EntityWithTableAnnotationAndEmptyAnnotationValue extends RawEntity<Object> {
    }

    private interface EntityWithNoTableAnnotation extends RawEntity<Object> {
    }
}
