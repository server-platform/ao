package net.java.ao.it.model;

import net.java.ao.Entity;
import net.java.ao.OneToMany;


public interface Score extends Entity {
    String getPlayer();
    void setPlayer(String competitor);

    String getGame();
    void setGame(String game);

    void setScore(int score);
    int getScore();
}
