package net.java.ao.it.model;

import net.java.ao.schema.Table;

@Table("SCORE")
public interface ScoreWithCount extends Score {
    void setCount(int count);
    int getCount();
}
