package net.java.ao.test.jdbc;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import static org.apache.commons.lang3.ArrayUtils.toArray;

/**
 * This class provides common H2 configuration settings.
 * Mainly, it helps with formatting the connection URL, which is H2 version dependant.
 */
public abstract class H2 extends AbstractJdbcConfiguration {

    private static final String DEFAULT_USER = "";
    private static final String DEFAULT_PASSWORD = "";
    private static final String DEFAULT_SCHEMA = "PUBLIC";
    private static final Integer[] VERSION = initH2Version();

    private static Integer[] initH2Version() {
        // We have to read H2 version constants via reflection to
        // prevent compiler from embedding the values at compile time.

        try {
            final Class<?> H2Constants = Class.forName("org.h2.engine.Constants");

            return toArray(
                    (Integer)H2Constants.getDeclaredField("VERSION_MAJOR").get(null),
                    (Integer)H2Constants.getDeclaredField("VERSION_MINOR").get(null),
                    (Integer)H2Constants.getDeclaredField("BUILD_ID").get(null));
        } catch (ClassNotFoundException e) {
            // We're not running H2 test, so driver is not available.
            return toArray(0, 0, 0);
        } catch (Exception e) {
            // Report all other errors.
            throw new RuntimeException(e);
        }
    }

    protected static int h2VersionCompareTo(int major, int minor, int build) {
        final Integer[] target = toArray(major, minor, build);

        // Lexicographical order
        int result = 0;
        for (int i = 0; i < VERSION.length; i++) {
            result = VERSION[i].compareTo(target[i]);
            if (result != 0) {
                break;
            }
        }

        return result;
    }

    protected static List<String> driverSettings() {
        List<String> parameters = new ArrayList<>();

        // H2 1.4.198+ removed the 'MVCC' parameter and
        // since 1.4.200 throws an error if it is specified.
        if (h2VersionCompareTo(1, 4, 198) < 0) {
            parameters.add("MVCC=TRUE");
        }

        return parameters;
    }

    protected static void appendDriverSettings(StringBuilder url) {
        StringJoiner joiner = new StringJoiner(";", ";", "").setEmptyValue("");
        driverSettings().forEach(joiner::add);
        url.append(joiner.toString());
    }

    public H2(String url) {
        this(url, DEFAULT_USER, DEFAULT_PASSWORD, DEFAULT_SCHEMA);
    }

    public H2(String url, String username, String password, String schema) {
        super(url, username, password, schema);
    }

    @Override
    protected String getDefaultUsername() {
        return DEFAULT_USER;
    }

    @Override
    protected String getDefaultPassword() {
        return DEFAULT_PASSWORD;
    }

    @Override
    protected String getDefaultSchema() {
        return DEFAULT_SCHEMA;
    }
}
