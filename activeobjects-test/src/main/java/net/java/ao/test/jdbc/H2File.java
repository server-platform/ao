package net.java.ao.test.jdbc;

import com.google.common.io.Files;

import java.io.File;

/**
 * Configuration settings for the H2 instance backed by file storage.
 * The file is created in a temporary folder to ensure uniqueness between
 * test runs.
 */
public class H2File extends H2 {
    private static final File TEMP_DIR = Files.createTempDir();
    private static final String DEFAULT_URL = makeDefaultUrl();

    private static String makeDefaultUrl() {
        StringBuilder url = new StringBuilder("jdbc:h2:file:");
        url.append(TEMP_DIR).append("/ao-test");
        appendDriverSettings(url);
        return url.toString();
    }

    public H2File() {
        super(DEFAULT_URL);
    }

    public H2File(String url, String username, String password, String schema) {
        super(url, username, password, schema);
    }

    @Override
    protected String getDefaultUrl() {
        return DEFAULT_URL;
    }
}
