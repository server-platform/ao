package net.java.ao.test.jdbc;

/**
 * Configuration settings for the H2 instance backed by memory.
 */
public class H2Memory extends H2 {
    private static final String DEFAULT_URL = makeDefaultUrl();

    private static String makeDefaultUrl() {
        StringBuilder url = new StringBuilder("jdbc:h2:mem:ao-test");
        appendDriverSettings(url);
        return url.toString();
    }

    public H2Memory() {
        super(DEFAULT_URL);
    }

    public H2Memory(String url, String username, String password, String schema) {
        super(url, username, password, schema);
    }

    @Override
    protected String getDefaultUrl() {
        return DEFAULT_URL;
    }
}
